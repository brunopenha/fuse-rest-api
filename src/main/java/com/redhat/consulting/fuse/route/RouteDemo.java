package com.redhat.consulting.fuse.route;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.redhat.consulting.fuse.Application;

@Component
public class RouteDemo extends RouteBuilder {
	private static Logger LOGGER = LoggerFactory.getLogger(Application.class);
	
	@Value("${fis.platform}")
	private String test;
	
	@Override
	public void configure() throws Exception {
		
        /*
         * O proposito desta rota é gerar metricas para o prometheus+grafana   
         */
        from("timer://foo?fixedRate=true&period=5000").routeId("rotateste")
        .process("processorDeleteme");

        
        /*
         * O propósio desta rota é gerar error para serem capturados pelo prometheus+grafana
         */
        from("timer://foo?fixedRate=true&period=60000").routeId("rotateste-erro")
        .process("processorDeletemeErro");
        
	
		
  
	}
}
