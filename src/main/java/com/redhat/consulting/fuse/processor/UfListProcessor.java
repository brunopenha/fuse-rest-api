package com.redhat.consulting.fuse.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.redhat.consulting.fuse.model.UfModel;

@Component
public class UfListProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		List<UfModel> list = new ArrayList<UfModel>();
		
	    list.add(new UfModel(1,"Acre", "AC"));
		list.add(new UfModel(2,"Alagoas","AL"));
		list.add(new UfModel(3,"Amapá", "AP"));
		list.add(new UfModel(4,"Amazonas", "AM"));
		list.add(new UfModel(5,"Bahia", "BA"));
		list.add(new UfModel(6,"Ceará", "CE"));
		list.add(new UfModel(7,"Distrito Federal", "DF"));
		list.add(new UfModel(8,"Espírito Santo", "ES"));
		list.add(new UfModel(9,"Goiás", "GO"));
		list.add(new UfModel(10,"Maranhão","MA"));
		list.add(new UfModel(11,"Mato Grosso","MT"));
		list.add(new UfModel(12,"Mato Grosso do Sul","MS"));
		list.add(new UfModel(13,"Minas Gerais","MG"));
		list.add(new UfModel(14,"Pará","PA"));
		list.add(new UfModel(15,"Paraíba","PB"));
		list.add(new UfModel(16,"Paraná","PR"));
		list.add(new UfModel(17,"Pernambuco","PE"));
		list.add(new UfModel(18,"Piauí","PI"));
		list.add(new UfModel(19,"Rio de Janeiro","RJ"));
		list.add(new UfModel(20,"Rio Grande do Norte","RN"));
		list.add(new UfModel(21,"Rio Grande do Sul"," RS"));
		list.add(new UfModel(22,"Rondônia","RO"));
		list.add(new UfModel(23,"Roraima"," RR"));
		list.add(new UfModel(24,"Santa Catarina","SC"));
		list.add(new UfModel(25,"São Paulo","SP"));
		list.add(new UfModel(26,"Tocantins","TO"));
				
		exchange.getIn().setBody(list);
		
	}

}
