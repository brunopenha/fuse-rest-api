package com.redhat.consulting.fuse.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ProcessorDeletemeErro implements Processor {
	private static Logger LOGGER = LoggerFactory.getLogger(ProcessorDeletemeErro.class);
		
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		LOGGER.info(" EXeCUTADO PROCESSOR COM ERRO");
		
		throw new Exception("EXCEPTION TEST");
		
		
	}
	
	
}
