package com.redhat.consulting.fuse.processor;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.redhat.consulting.fuse.model.PessoaModel;

@Component
public class InsertProcessor implements Processor {
 	
	@Value("${example.property}")
 	private String name;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		PessoaModel pessoa = exchange.getIn().getBody(PessoaModel.class);
		pessoa.set_id(String.valueOf(new Date().getTime()));		
		exchange.getIn().setBody(pessoa);
		
	}
	
	
}
