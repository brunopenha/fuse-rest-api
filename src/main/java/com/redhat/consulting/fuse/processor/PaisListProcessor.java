package com.redhat.consulting.fuse.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.redhat.consulting.fuse.model.PaisModel;

@Component
public class PaisListProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		List<PaisModel> countries = new ArrayList<PaisModel>();
	
		PaisModel country = new PaisModel();
		country.setId("BR");
		country.setName("Brazil");
		
		countries.add(country);
		
		country = new PaisModel();
		country.setId("US");
		country.setName("United States");
		
		countries.add(country);
		
		exchange.getIn().setBody(countries);
		
	}

}
