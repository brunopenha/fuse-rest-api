package com.redhat.consulting.fuse.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.redhat.consulting.fuse.model.PessoaModel;


@Component
public class PessoaFindAllProcessor implements Processor {
 	
	@Value("${example.property}")
 	private String name;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		
	 java.util.List<BasicDBObject> list = exchange.getIn().getBody(List.class);
	 
	 List<PessoaModel> listModel = new ArrayList<PessoaModel>();
	 for (BasicDBObject item : list) {		 
		 listModel.add(
				 new PessoaModel(
								 item.get("_id"),
								 item.get("nome"),
								 item.get("email"),
								 item.get("cpf"),
								 item.get("celular"),
								 item.get("endereco"),
								 item.get("complemento"),
								 item.get("cidade"),
								 item.get("uf"),
								 item.get("cep"),
								 item.get("pais")
						 	)
				 );		 
		 
	 }
	 
	 exchange.getIn().setBody(listModel);
		
	}

}
