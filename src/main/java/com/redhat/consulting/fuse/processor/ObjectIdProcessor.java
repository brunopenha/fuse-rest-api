package com.redhat.consulting.fuse.processor;


import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

@Component
public class ObjectIdProcessor implements Processor {
 	
	@Value("${example.property}")
 	private String name;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		String id = exchange.getIn().getHeader("id", String.class);
		
		DBObject doc=new BasicDBObject();
		doc.put("_id", id);
		
		exchange.getIn().setBody(doc, DBObject.class );
		
	}
	
	
}
